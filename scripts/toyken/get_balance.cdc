import Toyken from "../../contracts/Toyken.cdc"
import FungibleToken from "../../contracts/FungibleToken.cdc"

// This script returns an account's Toyken balance.

pub fun main(address: Address): UFix64 {
    let account = getAccount(address)
    
    let vaultRef = account.getCapability(Toyken.BalancePublicPath)!.borrow<&Toyken.Vault{FungibleToken.Balance}>()
        ?? panic("Could not borrow Balance reference to the Vault")

    return vaultRef.balance
}
