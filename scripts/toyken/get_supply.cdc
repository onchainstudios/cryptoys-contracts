import Toyken from "../../contracts/Toyken.cdc"

// This script returns the total amount of Toyken currently in existence.

pub fun main(): UFix64 {

    let supply = Toyken.totalSupply

    log(supply)

    return supply
}
