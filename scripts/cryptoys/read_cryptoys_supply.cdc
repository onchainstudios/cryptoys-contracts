import Cryptoys from "../../contracts/Cryptoys.cdc"

// This scripts returns the number of Cryptoys currently in existence.

pub fun main(): UInt64 {    
    return Cryptoys.totalSupply
}
