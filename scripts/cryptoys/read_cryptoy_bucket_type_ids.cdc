import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"

pub fun main(address: Address, key: String, characterId: UInt64): [UInt64] {
    let collectionBorrow = getAccount(address).getCapability(Cryptoys.CollectionPublicPath)
        .borrow<&{ICryptoys.CryptoysCollectionPublic}>()
        ?? panic("Could not borrow CryptoysCollectionPublic")

    let cryptoyRef = collectionBorrow.borrowCryptoy(id: characterId)
    
    return cryptoyRef.getBucketResourceIdsByKey(key)
}
