import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"

pub fun main(address: Address, characterId: UInt64): [String] {
    let collectionBorrow = getAccount(address).getCapability(Cryptoys.CollectionPublicPath)
        .borrow<&{ICryptoys.CryptoysCollectionPublic}>()
        ?? panic("Could not borrow CryptoysCollectionPublic")

    // borrow a reference to a specific NFT in the collection
    let cryptoyRef = collectionBorrow.borrowCryptoy(id: characterId)
    
    return cryptoyRef.getBucketKeys()
}
