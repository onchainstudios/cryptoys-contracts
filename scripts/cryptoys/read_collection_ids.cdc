import Cryptoys from "../../contracts/Cryptoys.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

// This script returns an array of all the NFT IDs in an account's collection.

pub fun main(address: Address): [UInt64] {
    let account = getAccount(address)

    let collectionRef = account.getCapability<&{NonFungibleToken.CollectionPublic}>(Cryptoys.CollectionPublicPath)!
            .borrow()
            ?? panic("Could not borrow receiver reference")
    
    return collectionRef.getIDs()
}
