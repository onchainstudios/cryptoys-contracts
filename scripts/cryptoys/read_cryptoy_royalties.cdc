import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"

pub fun main(address: Address, itemID: UInt64): [ICryptoys.Royalty] {
    let collectionBorrow = getAccount(address).getCapability(Cryptoys.CollectionPublicPath)!
        .borrow<&{ICryptoys.CryptoysCollectionPublic}>()
        ?? panic("Could not borrow CryptoysCollectionPublic")

    // borrow a reference to a specific NFT in the collection
    let cryptoy = collectionBorrow.borrowCryptoy(id: itemID)

    return cryptoy.getRoyalties()
}
