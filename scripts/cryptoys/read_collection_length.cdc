import Cryptoys from "../../contracts/Cryptoys.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

// This script returns the size of an account's Cryptoys collection.

pub fun main(address: Address): Int {
    let account = getAccount(address)

    let collectionRef = account.getCapability<&{NonFungibleToken.CollectionPublic}>(Cryptoys.CollectionPublicPath)!
            .borrow()
            ?? panic("Could not borrow receiver reference")
    
    return collectionRef.getIDs().length
}
