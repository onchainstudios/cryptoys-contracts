import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"
import CryptoysMetadataView from "../../contracts/CryptoysMetadataView.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

pub fun main(address: Address, itemID: UInt64): CryptoysMetadataView.Cryptoy {
    let collectionBorrow = getAccount(address).getCapability(Cryptoys.CollectionPublicPath)!
        .borrow<&{ICryptoys.CryptoysCollectionPublic}>()
        ?? panic("Could not borrow CryptoysCollectionPublic")

    // borrow a reference to a specific NFT in the collection
    let cryptoy = collectionBorrow.borrowCryptoy(id: itemID)

    let view = cryptoy.resolveView(Type<CryptoysMetadataView.Cryptoy>()) 
        ?? panic("cryptoy view not found")

    let metadata = view as! CryptoysMetadataView.Cryptoy
    
    return metadata
}
