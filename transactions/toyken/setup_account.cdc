import FungibleToken from "../../contracts/FungibleToken.cdc"
import Toyken from "../../contracts/Toyken.cdc"

// This transaction is a template for a transaction
// to add a Vault resource to their account
// so that they can use the Toyken

transaction {

    prepare(signer: AuthAccount) {

        if signer.borrow<&Toyken.Vault>(from: Toyken.VaultStoragePath) == nil {
            // Create a new Toyken Vault and put it in storage
            signer.save(<-Toyken.createEmptyVault(), to: Toyken.VaultStoragePath)

            // Create a public capability to the Vault that only exposes
            // the deposit function through the Receiver interface
            signer.link<&Toyken.Vault{FungibleToken.Receiver}>(
                Toyken.ReceiverPublicPath,
                target: Toyken.VaultStoragePath
            )

            // Create a public capability to the Vault that only exposes
            // the balance field through the Balance interface
            signer.link<&Toyken.Vault{FungibleToken.Balance}>(
                Toyken.BalancePublicPath,
                target: Toyken.VaultStoragePath
            )
        }
    }
}
