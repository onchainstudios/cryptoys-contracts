import Cryptoys from "../../contracts/Cryptoys.cdc"
transaction(name: String, address: Address, fee: UFix64){
    
    // local variable for storing the admin reference
    let admin: &Cryptoys.Admin

    prepare(signer: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.admin = signer.borrow<&Cryptoys.Admin>(from: Cryptoys.AdminStoragePath)
            ?? panic("Could not borrow a reference to the NFT admin")
    }

    execute {        
        self.admin.upsertRoyalty(
            royalty: Cryptoys.Royalty(name: name, address: address, fee: fee)
        )
    }

}
