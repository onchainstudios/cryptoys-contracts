import FungibleToken from "../../contracts/FungibleToken.cdc"

transaction(address: Address, amount: UFix64) {
    prepare(signer: AuthAccount) {
        let acct = getAccount(address)

        let vault <- signer // send depositAmount to temporary vault
            .borrow<&{FungibleToken.Provider}>(from: /storage/flowTokenVault)!
            .withdraw(amount: amount)

        acct.getCapability(/public/flowTokenReceiver)! // send temporary vault funds to new account
            .borrow<&{FungibleToken.Receiver}>()!
            .deposit(from: <- vault)
    }
}