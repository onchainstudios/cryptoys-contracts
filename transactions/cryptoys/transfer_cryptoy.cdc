import Cryptoys from "../../contracts/Cryptoys.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

// This transaction transfers a Cryptoy from one account to another.

transaction(recipient: Address, withdrawID: UInt64) {
    let token: @NonFungibleToken.NFT
    let receiver: Capability<&{NonFungibleToken.CollectionPublic}>

    prepare(signer: AuthAccount) {
        let collection = signer.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath)
            ?? panic("could not borrow SoftCollection collection from account")
        self.token <- collection.withdraw(withdrawID: withdrawID)
        self.receiver = getAccount(recipient).getCapability<&{NonFungibleToken.CollectionPublic}>(Cryptoys.CollectionPublicPath)
    }

    execute {
        let receiver = self.receiver.borrow()
            ?? panic("recipient Cryptoys collection not initialized")
        receiver.deposit(token: <- self.token)
    }
}
