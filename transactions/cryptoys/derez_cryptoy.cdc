import Cryptoys from "../../contracts/Cryptoys.cdc"

transaction(itemID: UInt64) {
    let collectionRef: &Cryptoys.Collection

    prepare(signer: AuthAccount) {
        self.collectionRef = signer.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath)
            ?? panic("Could not borrow a reference to the owner's collection")
    }

    execute {
        let burnItem <- self.collectionRef.withdraw(withdrawID: itemID)
        destroy burnItem
    }
}