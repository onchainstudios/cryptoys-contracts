import Cryptoys from "../../contracts/Cryptoys.cdc"

transaction(key: String, characterId: UInt64, itemId: UInt64) {
    let collectionRef: &Cryptoys.Collection
    prepare(signer: AuthAccount) {
        self.collectionRef = signer.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath)
            ?? panic("Could not borrow a reference to the owner's collection")
    }
    execute{
        let character = self.collectionRef.borrowCryptoy(id: characterId)
        let item <- self.collectionRef.withdraw(withdrawID: itemId) as! @Cryptoys.NFT
        character.addToBucket(key, <-item)
    }
}