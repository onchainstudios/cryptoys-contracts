import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"

transaction(owner: Address, id: UInt64, image: String, video: String){
    
    // local variable for storing the admin reference
    let admin: &Cryptoys.Admin
    let cryptoy: &AnyResource{ICryptoys.INFT}
    let display: Cryptoys.Display?

    prepare(signer: AuthAccount) {
        // borrow a reference to the Admin resource in storage
        self.admin = signer.borrow<&Cryptoys.Admin>(from: Cryptoys.AdminStoragePath)
            ?? panic("Could not borrow a reference to the NFT admin")

        let collectionBorrow = getAccount(owner).getCapability(Cryptoys.CollectionPublicPath)!
            .borrow<&{ICryptoys.CryptoysCollectionPublic}>()
            ?? panic("Could not borrow CryptoysCollectionPublic")

        // borrow a reference to a specific NFT in the collection
        self.cryptoy = collectionBorrow.borrowCryptoy(id: id)
        if image != "" && video != "" {
            self.display = Cryptoys.Display(image: image, video: video)
        }else{
            self.display = nil
        }
    }

    execute {        
        self.admin.updateDisplay(
            cryptoy: self.cryptoy,
            display: self.display
        )
    }
}

