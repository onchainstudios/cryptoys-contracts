import Cryptoys from "../../contracts/Cryptoys.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

transaction(key: String, parentId: UInt64, itemUuid: UInt64) {
    prepare(signer: AuthAccount) {
        let collectionRef = signer.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath)
            ?? panic("Could not borrow a reference to the owner's collection")

        let token <- collectionRef.withdrawBucketItem(parentId: parentId, key: key, itemUuid: itemUuid) as! @NonFungibleToken.NFT

        collectionRef.deposit(token:<-token)
    }
}
