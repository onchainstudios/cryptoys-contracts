import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"
import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"

// This transaction configures an account to hold Cryptoys Items.

transaction {
    prepare(signer: AuthAccount) {
        // if the account doesn't already have a collection
        if signer.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath) == nil {

            // create a new empty collection
            let collection <- Cryptoys.createEmptyCollection()
            
            // save it to the account
            signer.save(<-collection, to: Cryptoys.CollectionStoragePath)

            // create a public capability for the collection
            signer.link<&Cryptoys.Collection{NonFungibleToken.CollectionPublic, ICryptoys.CryptoysCollectionPublic}>(Cryptoys.CollectionPublicPath, target: Cryptoys.CollectionStoragePath)
        }
    }
}
