import Cryptoys from "../../contracts/Cryptoys.cdc"
import ICryptoys from "../../contracts/ICryptoys.cdc"
import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"

transaction(recipient: Address, metadata: {String: String}, items: [{String: String}], royalties: [String], imageWithPossessions: String){
    let admin: &Cryptoys.Admin
    let receiver: Capability<&{NonFungibleToken.CollectionPublic}>
    let contractCollection: Capability<&{NonFungibleToken.CollectionPublic}>
    let collectionRef: &Cryptoys.Collection
    prepare(account: AuthAccount) {
        self.admin = account.borrow<&Cryptoys.Admin>(from: Cryptoys.AdminStoragePath)!
        if !account.getCapability<&{NonFungibleToken.CollectionPublic,NonFungibleToken.Receiver}>(Cryptoys.CollectionPublicPath).check() {
            if account.borrow<&AnyResource>(from: Cryptoys.CollectionStoragePath) != nil {
                account.unlink(Cryptoys.CollectionPublicPath)
                account.link<&{NonFungibleToken.CollectionPublic,NonFungibleToken.Receiver}>(Cryptoys.CollectionPublicPath, target: Cryptoys.CollectionStoragePath)
            } else {
                let collection <- Cryptoys.createEmptyCollection() as! @Cryptoys.Collection
                account.save(<-collection, to: Cryptoys.CollectionStoragePath)
                account.link<&{NonFungibleToken.CollectionPublic,NonFungibleToken.Receiver}>(Cryptoys.CollectionPublicPath, target: Cryptoys.CollectionStoragePath)
            }
        }
        self.receiver = getAccount(recipient).getCapability<&{NonFungibleToken.CollectionPublic}>(Cryptoys.CollectionPublicPath)
        self.contractCollection = account.getCapability<&{NonFungibleToken.CollectionPublic}>(Cryptoys.CollectionPublicPath)

        self.collectionRef = account.borrow<&Cryptoys.Collection>(from: Cryptoys.CollectionStoragePath)
            ?? panic("Could not borrow a reference to the owner's collection")
    }
    execute {
        if items.length == 0 {
            self.admin.mintNFT(
                recipient: self.receiver, 
                metadata:  metadata,
                royaltyNames: royalties
            )
        } else {
            let nftId = self.admin.mintNFT(
                recipient: self.contractCollection, 
                metadata:  metadata,
                royaltyNames: royalties
            )

            let nftRef = self.collectionRef.borrowCryptoy(id: nftId)

            for itemMetadata in items {
                let itemId = self.admin.mintNFT(
                    recipient: self.contractCollection, 
                    metadata:  itemMetadata,
                    royaltyNames: royalties
                )

                let item <- self.collectionRef.withdraw(withdrawID: itemId) as! @Cryptoys.NFT
                nftRef.addToBucket("item", <-item)
            }

            let nft <- self.collectionRef.withdraw(withdrawID: nftId) as! @Cryptoys.NFT

            self.receiver.borrow()!.deposit(token: <- nft)

            if imageWithPossessions.length > 0 {
                let display: Cryptoys.Display? = Cryptoys.Display(image: imageWithPossessions, video: "")

                self.admin.updateDisplay(
                    cryptoy: nftRef,
                    display: display
                )
            }
        }
    }
}
 