transaction(contractName: String, contractCode: String) {
	prepare(signer: AuthAccount) {
        signer.contracts.add(name: contractName, code: contractCode.decodeHex())
	}
}