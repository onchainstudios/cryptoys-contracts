import Cryptoys from "../../contracts/Cryptoys.cdc"

transaction() {
    let admin: &Cryptoys.Admin

    prepare(account: AuthAccount) {
        self.admin = account.borrow<&Cryptoys.Admin>(from: Cryptoys.AdminStoragePath)!
    }
    execute {        
        log(self.admin.getRoyalties())
    }
}
