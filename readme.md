# Start emulator and deploy contracts

### Start emulator using settings in flow.json
`flow emulator`

### Deploy contracts in emulator for service account. (configured in flow.json)
`flow project deploy`
