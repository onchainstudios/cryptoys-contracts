#!/bin/bash
set -e

# go’s coverage generator completely omits coverage data for directories that don’t contain any tests (even though there are source files in them),
# which causes the overall coverage percentage to be artificially high.
# So this script does some tomfoolery to detect directories with source files but no tests and creates an empty dummy test and runs it in order
# to generate a 0% code coverage hitmap.
# There’s a 4-year-old, still-unresolved issue on github complaining about this problem: https://github.com/golang/go/issues/24570

CALLING_DIR="`pwd`"

WORKING_DIR="${1:-./}"
cd "$WORKING_DIR"

OUTPUT_FILE="$(realpath "${CALLING_DIR}/coverage.out")"

echo 'mode: set' > "$OUTPUT_FILE"

anyFound=0
for dir in $(find ./ -maxdepth 1 -not -path './.git*' -not -path '*/_*' -not -path './submodules*' -not -path './.*' -not -path './static*' -type d);
do
    basename="$(basename "$(realpath "$dir")")"
    profile="coverage.$basename.tmp"
    hasGoFiles="$(find "./$dir" -maxdepth 1 -type f -iname '*.go' -not -iname '*_test.go' | wc -l)"
    hasTestFiles="$(find "./$dir" -maxdepth 1 -type f -iname '*_test.go' | wc -l)"
    if [[ hasGoFiles -ne 0 ]]; then
        anyFound=1
        cleanup=0
        if [[ hasTestFiles -eq 0 ]]; then
            if [[ "$dir" == "." || "$dir" == "./" ]]; then
                basename="main"
            fi
            echo -e "package $basename\n\nimport \"testing\"\n\nfunc TestFoobar(t *testing.T) {\n}\n" > "$dir/temp_test.go"
            cleanup=1
        else
            echo 'Running tests in `./'"$(realpath --relative-to="$CALLING_DIR" "$dir")"'/..`'
        fi

        go test -short -covermode=set -coverprofile="$profile" "$dir"
        if [ -f "$profile" ]; then
            cat "$profile" | tail -n +2 >> "$OUTPUT_FILE"
            rm "$profile"
        fi

        if [[ cleanup -eq 1 ]]; then
            rm "$dir/temp_test.go"
        fi
    fi
done

if [[ anyFound -eq 0 ]]; then
    echo "No tests/packages"
fi
