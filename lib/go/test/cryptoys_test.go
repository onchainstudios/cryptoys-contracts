package test

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"testing"
	"time"

	"bitbucket.org/onchainstudios/go-common/models"
	"github.com/bxcodec/faker/v3"
	"github.com/onflow/cadence"
	jsoncdc "github.com/onflow/cadence/encoding/json"
	emulator "github.com/onflow/flow-emulator"
	"github.com/onflow/flow-emulator/types"
	"github.com/onflow/flow-go-sdk/crypto"
	sdktemplates "github.com/onflow/flow-go-sdk/templates"
	"github.com/onflow/flow-go-sdk/test"
	"github.com/stretchr/testify/require"
	"github.com/onflow/flow-go/fvm"
	"github.com/stretchr/testify/assert"
	"github.com/onflow/flow-go-sdk"
	nft_contracts "github.com/onflow/flow-nft/lib/go/contracts"
)

func init() {
    rand.Seed(time.Now().UnixNano())
}

const (
	cryptoysTransactionsRootPath = "../../../transactions/cryptoys"
	cryptoysScriptsRootPath      = "../../../scripts/cryptoys"

	fungibleTokenContractPath           = "../../../contracts/FungibleToken.cdc"
	flowTokenContractPath               = "../../../contracts/FlowToken.cdc"
	cryptoysContractPath                = "../../../contracts/Cryptoys.cdc"
	cryptoysMetadataViewContractPath    = "../../../contracts/CryptoysMetadataView.cdc"
	cryptoysInterfaceContractPath       = "../../../contracts/ICryptoys.cdc"
	cryptoysNewAccountPath              = cryptoysTransactionsRootPath + "/new_cryptoy_account.cdc"
	cryptoysMintCryptoyPath             = cryptoysTransactionsRootPath + "/mint_cryptoy.cdc"
	cryptoysAddContractPath             = cryptoysTransactionsRootPath + "/add_contract.cdc"
	CryptoysCreateRoyaltyPath           = cryptoysTransactionsRootPath + "/upsert_royalty.cdc"
	cryptoysTransferCryptoyPath         = cryptoysTransactionsRootPath + "/transfer_cryptoy.cdc"
	cryptoysComposeCryptoyPath          = cryptoysTransactionsRootPath + "/compose_cryptoy.cdc"
	cryptoysDiscomposeCryptoyPath       = cryptoysTransactionsRootPath + "/discompose_cryptoy.cdc"
	cryptoysInspectRoyaltiesPath        = cryptoysTransactionsRootPath + "/read_royalties.cdc"
	cryptoysUpdateDisplayPath           = cryptoysTransactionsRootPath + "/update_display.cdc"
	sendFlowToAccount                   = cryptoysTransactionsRootPath + "/send_flow_to_account.cdc"
	cryptoysInspectCryptoySupplyPath    = cryptoysScriptsRootPath + "/read_cryptoys_supply.cdc"
	cryptoysInspectCollectionLenPath    = cryptoysScriptsRootPath + "/read_collection_length.cdc"
	cryptoysInspectCollectionIdsPath    = cryptoysScriptsRootPath + "/read_collection_ids.cdc"
	cryptoysInspectCryptoyMetadataPath  = cryptoysScriptsRootPath + "/read_cryptoy_data.cdc"
	cryptoysInspectCryptoyRoyaltiesPath = cryptoysScriptsRootPath + "/read_cryptoy_royalties.cdc"
	cryptoysReadBucketItem              = cryptoysScriptsRootPath + "/read_cryptoy_bucket_item.cdc"
	cryptoysReadBucketTypeIds           = cryptoysScriptsRootPath + "/read_cryptoy_bucket_type_ids.cdc"
	cryptoysReadBucketTypes             = cryptoysScriptsRootPath + "/read_cryptoy_bucket_types.cdc"
	readUserAccountRemainingStorage     = cryptoysScriptsRootPath + "/read_remaining_account_storage.cdc"
)

type EventsTest struct {
	eventName string
	arguments []interface{}
}

func FakeNftMetadata() models.NFTMeta {
	nftMetaData := models.NFTMeta{}
	faker.FakeData(&nftMetaData)

	return models.NFTMeta{
		PlatformID: "PlatformID_"+nftMetaData.PlatformID,
		Category:   "Category_"+nftMetaData.Category,
		Type:       "Type_"+nftMetaData.Type,
		Skin:       "Skin_"+nftMetaData.Skin,
		Tier:       "Tier_"+nftMetaData.Tier,
		Rarity:     "Rarity_"+nftMetaData.Rarity,
		Edition:     nftMetaData.Edition,
		Series:      "Series_"+nftMetaData.Series,
		LegionId:    "LegionId_"+nftMetaData.LegionId,
		Creator:     "Creator_"+nftMetaData.Creator,
		Packaging:   "Packaging_"+nftMetaData.Packaging,
		TermsUrl:    "TermsUrl_"+nftMetaData.TermsUrl,
		Description: "Description_"+nftMetaData.Description,
		Image:       "Image_"+nftMetaData.Image,
		CoreImage:   "CoreImage_"+nftMetaData.CoreImage,
		Video:       "Video_"+nftMetaData.Video,
	}
}

func NftMetadataToMetadataView(nftMetadata models.NFTMeta) []string {
	return []string{ // -- Order matters here and must match CryptoysMetadataView.cdc
		nftMetadata.Type,
		nftMetadata.Description,
		nftMetadata.Image,
		nftMetadata.CoreImage,
		nftMetadata.Video,
		nftMetadata.PlatformID,
		nftMetadata.Category,
		nftMetadata.Type,
		nftMetadata.Skin,
		nftMetadata.Tier,
		nftMetadata.Rarity,
		fmt.Sprintf("%d", nftMetadata.Edition),
		nftMetadata.Series,
		nftMetadata.LegionId,
		nftMetadata.Creator,
		nftMetadata.Packaging,
		nftMetadata.TermsUrl,
	}
}

func CryptoysDeployContracts(b *emulator.Blockchain, t *testing.T) (ContractAddresses, crypto.Signer) {
	fungibleTokenAddress := flow.HexToAddress(fvm.FungibleTokenAddress(b.GetChain()).Hex())

	nftAddress, err := b.CreateAccount(
		nil,
		[]sdktemplates.Contract{
			{
				Name:   "NonFungibleToken",
				Source: string(nft_contracts.NonFungibleToken()),
			},
		},
	)
	require.NoError(t, err)

	metadataViewsAddress, err := b.CreateAccount(
		nil,
		[]sdktemplates.Contract{
			{
				Name:   "MetadataViews",
				Source: string(nft_contracts.MetadataViews(fungibleTokenAddress, nftAddress)),
			},
		},
	)
	require.NoError(t, err)

	_, err = b.CommitBlock()
	assert.NoError(t, err)

	contracts := ContractAddresses{
		fungibleToken: fungibleTokenAddress,
		nonFungibleToken: nftAddress,
		metadataViews: metadataViewsAddress,
		flowToken: flow.HexToAddress(fvm.FlowTokenAddress(b.GetChain()).Hex()),
		cryptoysMetadataView: metadataViewsAddress,
	}

	accountKeys := test.AccountKeyGenerator()
	// should be able to deploy a contract as a new account with one key
	cryptoysAccountKey, cryptoysSigner := accountKeys.NewWithSigner()

	cryptoysAddress, err := b.CreateAccount(
		[]*flow.AccountKey{cryptoysAccountKey},
		[]sdktemplates.Contract{
			{
				Name:   "CryptoysMetadataView",
				Source: string(readFile(cryptoysMetadataViewContractPath)),
			},
		},
	)
	contracts.cryptoysMetadataView = cryptoysAddress

	assert.NoError(t, err)

	_, err = b.CommitBlock()
	assert.NoError(t, err)

	CryptoysAddContract(b, t, cryptoysAddress, cryptoysSigner, "ICryptoys", string(contracts.ReplaceInCode(cryptoysInterfaceContractPath)))
	contracts.iCryptoys = cryptoysAddress

	CryptoysAddContract(b, t, cryptoysAddress, cryptoysSigner, "Cryptoys", string(contracts.ReplaceInCode(cryptoysContractPath)))
	contracts.cryptoys = cryptoysAddress

	_, err = b.CommitBlock()
	assert.NoError(t, err)

	return contracts, cryptoysSigner
}

func CryptoysAddContract(b *emulator.Blockchain, t *testing.T, signerAddress flow.Address, signerSignature crypto.Signer, contractName string, contractCode string) {
	tx := flow.NewTransaction().
		SetScript(readFile(cryptoysAddContractPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(signerAddress)

	tx.AddArgument(NewCadenceString(contractName))
	tx.AddArgument(NewCadenceString(hex.EncodeToString([]byte(contractCode))))

	serviceKeySigner, _ := b.ServiceKey().Signer()
	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, signerAddress},
		[]crypto.Signer{serviceKeySigner, signerSignature},
		false,
	)

}

func CryptoysSetupAccount(
	t *testing.T, b *emulator.Blockchain, key1 *flow.AccountKey, key2 *flow.AccountKey, contracts ContractAddresses, flowTransferAmount cadence.UFix64,
)  *types.TransactionResult {
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysNewAccountPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(b.ServiceKey().Address)

	tx.AddArgument(cadence.String(hex.EncodeToString(key1.Encode())))
	tx.AddArgument(cadence.String(hex.EncodeToString(key2.Encode())))
	tx.AddArgument(cadence.Value(flowTransferAmount))

	serviceKeySigner, _ := b.ServiceKey().Signer()

	return signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address},
		[]crypto.Signer{serviceKeySigner},
		false,
	)
}

func CryptoysMint(
	t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
	nftMetadata models.NFTMeta,
	itemsMetadata []models.NFTMeta,
	nftImageWithPossessions string,
	royalties models.Royalties,
	recipientAddress flow.Address,
) (id cadence.UInt64, uuid cadence.UInt64) {

	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysMintCryptoyPath)).
		SetGasLimit(999).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(contracts.cryptoys)

	tx.AddArgument(cadence.NewAddress(recipientAddress))

	metadata := nftMetadata.AsCadenceValues()
	metadataDictionary := cadence.NewDictionary(metadata)
	tx.AddArgument(metadataDictionary)

	items := []cadence.Value{}
	for _, itemMetadata := range itemsMetadata {
		items = append(items, cadence.NewDictionary(itemMetadata.AsCadenceValues()))
	}
	tx.AddArgument(cadence.NewArray(items))

	royaltyArgs := []cadence.Value{}
	expectedRoyaltyResults := []cadence.Value{}
	for _, royalty := range royalties {
		royaltyArgs = append(royaltyArgs, royalty.Name)

		expectedRoyaltyResults = append(expectedRoyaltyResults, royalty.AsCadenceStruct())
	}

	tx.AddArgument(cadence.NewArray(royaltyArgs))
	tx.AddArgument(cadence.String(nftImageWithPossessions))

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, contracts.cryptoys},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		false,
	)

	var parentNftId interface{}
	if len(result.Events) < 1 || result.Events[0].Type != fmt.Sprintf("A.%s.Cryptoys.Minted", contracts.cryptoys) {
		t.Fatal("Cryptoys.Minted event is not emitted")
	}else{
		parentNftId = result.Events[0].Value.Fields[0]
	}

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.Minted",
			arguments: []interface{}{
				reflect.TypeOf((*cadence.UInt64)(nil)).Elem(), // -- nftId value is unknown, pass the type to check if event types match
				reflect.TypeOf((*cadence.UInt64)(nil)).Elem(), // -- nftUuid value is unknown, pass the type to check if event types match
				metadataDictionary,
				cadence.NewArray(expectedRoyaltyResults),
			},
		},
	}

	if len(itemsMetadata) > 0 {
		expectedEvents = append(expectedEvents, EventsTest{
			eventName: "Cryptoys.Deposit",
			arguments: []interface{}{
				parentNftId,
				cadence.NewOptional(cadence.NewAddress(contracts.cryptoys)),
			},
		})

		for i, itemMetadata := range itemsMetadata {
			mintItemEventIndex := i + 2 + (i*3) // -- 2 events for parent nft, 4 events for each item

			var itemUuid interface{}
			var itemId   interface{}
			if len(result.Events) < mintItemEventIndex || result.Events[mintItemEventIndex].Type != fmt.Sprintf("A.%s.Cryptoys.Minted", contracts.cryptoys) {
				t.Fatalf("Cryptoys.Minted event is not emitted for item #%d", i+1)
			}else{
				itemId   = result.Events[mintItemEventIndex].Value.Fields[0]
				itemUuid = result.Events[mintItemEventIndex].Value.Fields[1]
			}

			expectedEvents = append(expectedEvents, []EventsTest{
				{
					eventName: "Cryptoys.Minted",
					arguments: []interface{}{
						reflect.TypeOf((*cadence.UInt64)(nil)).Elem(), // value is unknown, pass the type to check if event types match
						reflect.TypeOf((*cadence.UInt64)(nil)).Elem(), // value is unknown, pass the type to check if event types match
						cadence.NewDictionary(itemMetadata.AsCadenceValues()),
						cadence.NewArray(expectedRoyaltyResults),
					},
				},
				{
					eventName: "Cryptoys.Deposit",
					arguments: []interface{}{
						itemId,
						cadence.NewOptional(cadence.NewAddress(contracts.cryptoys)),
					},
				},
				{
					eventName: "Cryptoys.Withdraw",
					arguments: []interface{}{
						itemId,
						cadence.NewOptional(cadence.NewAddress(contracts.cryptoys)),
					},
				},
				{
					eventName: "Cryptoys.AddedToBucket",
					arguments: []interface{}{
						cadence.NewAddress(contracts.cryptoys),
						cadence.String("item"),
						parentNftId,
						itemUuid,
					},
				},
			}...)
		}

		expectedEvents = append(expectedEvents, EventsTest{
			eventName: "Cryptoys.Withdraw",
			arguments: []interface{}{
				parentNftId,
				cadence.NewOptional(cadence.NewAddress(contracts.cryptoys)),
			},
		})
	}

	expectedEvents = append(expectedEvents, EventsTest{
		eventName: "Cryptoys.Deposit",
		arguments: []interface{}{
			parentNftId,
			cadence.NewOptional(cadence.NewAddress(recipientAddress)),
		},
	})

	if nftImageWithPossessions != "" {
		expectedEvents = append(expectedEvents, EventsTest{
			eventName: "Cryptoys.DisplayUpdated",
			arguments: []interface{}{
				parentNftId,
				cadence.String(nftImageWithPossessions),
				cadence.String(nftMetadata.Video),
			},
		})
	}

	AssertEvents(t, result.Events, expectedEvents)

	nftUuid := result.Events[0].Value.Fields[1]

	return parentNftId.(cadence.UInt64), nftUuid.(cadence.UInt64)
}

func CryptoysValidateMetadata(t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
	nftId cadence.UInt64,
	nftData models.NFTMeta,
	address flow.Address,
) {
	// assert that the account collection is correct length
	nft := executeScriptAndCheck(
		t,
		b,
		contracts.ReplaceInCode(cryptoysInspectCryptoyMetadataPath),
		[][]byte{jsoncdc.MustEncode(
			cadence.NewAddress(address)),
			jsoncdc.MustEncode(nftId),
		},
	)

	metadataFields := nft.(cadence.Struct).Fields
	expectedMetadataValues := NftMetadataToMetadataView(nftData)
	i := 0
	for _, value := range expectedMetadataValues{
		var cadenceValue cadence.Value
		if value == "" {
			cadenceValue = cadence.Value(nil)
		} else {
			cadenceValue = cadence.String(value)
		}

		assert.Equal(t, cadence.NewOptional(cadenceValue), metadataFields[i])
		i++
	}
}

func CryptoysCreateRoyalty(
	t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
) models.Royalty {

	fee, _ := cadence.NewUFix64(fmt.Sprintf("%f", rand.Float64()/10))
	royalty := models.Royalty{
		Name:    NewCadenceString(faker.Word()),
		Address: cadence.BytesToAddress([]byte(strconv.Itoa(rand.Intn(999)))),
		Fee:     fee,
	}

	return CryptoysUpsertRoyalty(t, b, contracts, cryptoysSigner, royalty)
}

func CryptoysUpsertRoyalty(
	t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
	royalty models.Royalty,
) models.Royalty {

	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(CryptoysCreateRoyaltyPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(contracts.cryptoys)

	tx.AddArgument(royalty.Name)
	tx.AddArgument(royalty.Address)
	tx.AddArgument(royalty.Fee)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, contracts.cryptoys},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		false,
	)

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.RoyaltyUpserted",
			arguments: []interface{}{
				royalty.Name,
				royalty.AsCadenceStruct(),
			},
		},
	}

	AssertEvents(t, result.Events, expectedEvents)

	return royalty
}

func CryptoysUpdateDisplay(
	t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
	owner cadence.Address,
	nftId cadence.UInt64,
	image cadence.String,
	originalImage cadence.String,
	video cadence.String,
	originalVideo cadence.String,
) {

	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysUpdateDisplayPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(contracts.cryptoys)

	tx.AddArgument(owner)
	tx.AddArgument(nftId)
	tx.AddArgument(image)
	tx.AddArgument(video)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, contracts.cryptoys},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		false,
	)

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.DisplayUpdated",
			arguments: []interface{}{
				nftId,
			},
		},
	}

	if image != "" && video != "" {
		expectedEvents[0].arguments = append(expectedEvents[0].arguments, image)
		expectedEvents[0].arguments = append(expectedEvents[0].arguments, video)
	} else {
		expectedEvents[0].arguments = append(expectedEvents[0].arguments, originalImage)
		expectedEvents[0].arguments = append(expectedEvents[0].arguments, originalVideo)
	}

	AssertEvents(t, result.Events, expectedEvents)
}

func CryptoysReadRoyalties(
	t *testing.T,
	b *emulator.Blockchain,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
) (logs []string) {
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysInspectRoyaltiesPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(contracts.cryptoys)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, contracts.cryptoys},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		false,
	)

	return result.Logs
}

func CryptoysTransferItem(
	t *testing.T, b *emulator.Blockchain,
	owner flow.Address,
	contracts ContractAddresses,
	cryptoysSigner crypto.Signer,
	assetId cadence.UInt64,
	recipientAddr flow.Address,
	shouldFail bool,
) {

	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysTransferCryptoyPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(owner)

	tx.AddArgument(cadence.NewAddress(recipientAddr))
	tx.AddArgument(assetId)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, owner},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		shouldFail,
	)

	if shouldFail {
		return
	}

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.Withdraw",
			arguments: []interface{}{
				assetId,
				cadence.NewOptional(cadence.NewAddress(owner)),
			},
		},
		{
			eventName: "Cryptoys.Deposit",
			arguments: []interface{}{
				assetId,
				cadence.NewOptional(cadence.NewAddress(recipientAddr)),
			},
		},
	}

	AssertEvents(t, result.Events, expectedEvents)
}

func CryptoysComposeItem(
	t *testing.T, b *emulator.Blockchain,
	owner flow.Address,
	contracts ContractAddresses, cryptoysSigner crypto.Signer,
	characterId cadence.UInt64, itemId cadence.UInt64, itemUuid cadence.UInt64, shouldFail bool,
) {

	bucketType := cadence.String("items")
	txSigner := b.ServiceKey()
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysComposeCryptoyPath)).
		SetGasLimit(100).
		SetProposalKey(txSigner.Address, txSigner.Index, txSigner.SequenceNumber).
		SetPayer(txSigner.Address).
		AddAuthorizer(owner)

	tx.AddArgument(bucketType)
	tx.AddArgument(characterId)
	tx.AddArgument(itemId)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{txSigner.Address, owner},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		shouldFail,
	)

	if shouldFail {
		return
	}

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.Withdraw",
			arguments: []interface{}{
				itemId,
				cadence.NewOptional(cadence.NewAddress(owner)),
			},
		},
		{
			eventName: "Cryptoys.AddedToBucket",
			arguments: []interface{}{
				cadence.NewAddress(owner),
				bucketType,
				characterId,
				itemUuid,
			},
		},
	}

	AssertEvents(t, result.Events, expectedEvents)
}


func SendFlowToAccount(
	t *testing.T, b *emulator.Blockchain,
	contracts ContractAddresses,
	address flow.Address, amount uint,
) {
	txSigner := b.ServiceKey()
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(sendFlowToAccount)).
		SetGasLimit(100).
		SetProposalKey(txSigner.Address, txSigner.Index, txSigner.SequenceNumber).
		SetPayer(txSigner.Address).
		AddAuthorizer(b.ServiceKey().Address)

	expectedTransferAmount, _ := cadence.NewUFix64FromParts(0, amount)

	tx.AddArgument(cadence.Address(address))
	tx.AddArgument(expectedTransferAmount)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{txSigner.Address},
		[]crypto.Signer{serviceKeySigner},
		false,
	)

	expectedEvents := []EventsTest{
		{
			eventName: "FlowToken.TokensWithdrawn",
			arguments: []interface{}{
				expectedTransferAmount,
				cadence.NewOptional(cadence.NewAddress(b.ServiceKey().Address)),
			},
		},
		{
			eventName: "FlowToken.TokensDeposited",
			arguments: []interface{}{
				expectedTransferAmount,
				cadence.NewOptional(cadence.NewAddress(address)),
			},
		},
	}

	AssertEvents(t, result.Events, expectedEvents)
}

func CryptoysDiscomposeItem(
	t *testing.T, b *emulator.Blockchain,
	owner flow.Address,
	contracts ContractAddresses, cryptoysSigner crypto.Signer,
	characterId cadence.UInt64, itemId cadence.UInt64, itemUuid cadence.UInt64, shouldFail bool,
) {

	bucketType := cadence.String("items")

	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(cryptoysDiscomposeCryptoyPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(owner)

	tx.AddArgument(bucketType)
	tx.AddArgument(characterId)
	tx.AddArgument(itemUuid)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	result := signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, owner},
		[]crypto.Signer{serviceKeySigner, cryptoysSigner},
		shouldFail,
	)

	if shouldFail {
		return
	}

	expectedEvents := []EventsTest{
		{
			eventName: "Cryptoys.WithdrawnFromBucket",
			arguments: []interface{}{
				cadence.NewAddress(owner),
				bucketType,
				characterId,
				itemUuid,
			},
		},
		{
			eventName: "Cryptoys.Deposit",
			arguments: []interface{}{
				itemId,
				cadence.NewOptional(cadence.NewAddress(owner)),
			},
		},
	}
	AssertEvents(t, result.Events, expectedEvents)
}

func TestCryptoysDeployContracts(t *testing.T) {
	b := newEmulator()
	CryptoysDeployContracts(b, t)
}

func TestCreateCryptoy(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)

	supply := executeScriptAndCheck(
		t, b,
		contracts.ReplaceInCode(cryptoysInspectCryptoySupplyPath),
		nil,
	)
	assert.EqualValues(t, cadence.NewUInt64(0), supply)

	// assert that the account collection is empty
	length := executeScriptAndCheck(
		t,
		b,
		contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
		[][]byte{jsoncdc.MustEncode(cadence.NewAddress(contracts.cryptoys))},
	)

	assert.EqualValues(t, cadence.NewInt(0), length)

	royalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

	t.Run("Should be able to mint a cryptoy", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)
		id, _ := CryptoysMint(t, b, contracts, cryptoysSigner, FakeNftMetadata(), nil, "", models.Royalties{royalty}, recipient)

		// assert that the account collection is correct length
		length = executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(1), length)

		// assert that we can fetch nft id
		ids := executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCollectionIdsPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)

		expectedValue := cadence.NewArray([]cadence.Value{
			id,
		})

		expectedValue.ArrayType = cadence.NewVariableSizedArrayType(cadence.NewUInt64Type())
		assert.EqualValues(t, expectedValue, ids)
	})

	t.Run("Should be able to fetch cryptoys Metadataview", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)
		nftData := FakeNftMetadata()
		nftId, _ := CryptoysMint(t, b, contracts, cryptoysSigner, nftData, nil, "", models.Royalties{royalty}, recipient)

		CryptoysValidateMetadata(t, b, contracts, cryptoysSigner, nftId, nftData, recipient)
	})

	t.Run("Should be able to fetch royalties", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)

		nftData := FakeNftMetadata()
		id, _ := CryptoysMint(t, b, contracts, cryptoysSigner, nftData, nil, "", models.Royalties{royalty}, recipient)

		royalties := executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCryptoyRoyaltiesPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient)),
				jsoncdc.MustEncode(id)},
		)

		AssertCadenceValue(t, royalties, models.Royalties{royalty}.AsCadenceArray())

		// ======== Update royalty and check nft royalties again
		royalty.Fee, _ = cadence.NewUFix64(fmt.Sprintf("%f", rand.Float64()/100))
		royalty.Address = cadence.BytesToAddress([]byte(strconv.Itoa(rand.Intn(999))))
		CryptoysUpsertRoyalty(t, b, contracts, cryptoysSigner, royalty)

		updatedRoyalties := executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCryptoyRoyaltiesPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient)),
				jsoncdc.MustEncode(id)},
		)

		AssertCadenceValue(t, updatedRoyalties, models.Royalties{royalty}.AsCadenceArray())
	})

}

func NewCryptoysAccount(t *testing.T, b *emulator.Blockchain, contracts ContractAddresses) flow.Address {
	keyGenerator := test.AccountKeyGenerator()
	result := CryptoysSetupAccount(t, b, keyGenerator.New(), keyGenerator.New(), contracts, cadence.UFix64(0))

	userAddress  := flow.Address{}
	for _, event := range result.Events {
		if event.Type != flow.EventAccountCreated {
			continue
		}

		userAddress = flow.AccountCreatedEvent(event).Address()
	}

	return userAddress
}


func TestCreateCryptoyWithItems(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)

	supply := executeScriptAndCheck(
		t, b,
		contracts.ReplaceInCode(cryptoysInspectCryptoySupplyPath),
		nil,
	)
	assert.EqualValues(t, cadence.NewUInt64(0), supply)

	royalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

	t.Run("Should be able to mint a cryptoy with items", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)
		length := executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
	
		assert.EqualValues(t, cadence.NewInt(0), length)

		nft := FakeNftMetadata()
		items := []models.NFTMeta{}
		for i := 0; i < 1; i++ {
			items = append(items, FakeNftMetadata())
		}

		id, _ := CryptoysMint(t, b, contracts, cryptoysSigner, nft, items, faker.URL(), models.Royalties{royalty}, recipient)

		// assert that the account collection is correct length
		length = executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(1), length)

		// assert that we can fetch nft id
		ids := executeScriptAndCheck(
			t,
			b,
			contracts.ReplaceInCode(cryptoysInspectCollectionIdsPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)

		expectedValue := cadence.NewArray([]cadence.Value{
			id,
		})

		expectedValue.ArrayType = cadence.NewVariableSizedArrayType(cadence.NewUInt64Type())
		assert.EqualValues(t, expectedValue, ids)
	})
}

func TestCryptoyRoyalties(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)

	// create a new Collection
	t.Run("Should be able to create royalties", func(t *testing.T) {
		// ======== create royalty
		expectedRoyalties := models.Royalties{
			CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner),
			CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner),
			CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner),
		}

		// ======== read royalty.  No guarantee of order for royalties printed in log
		transactionLogs := CryptoysReadRoyalties(t, b, contracts, cryptoysSigner)
		assert.Len(t, transactionLogs, 1)

		contractRoyaltyNameRegex := fmt.Sprintf("A\\.%s\\.Cryptoys\\.Royalty", contracts.cryptoys.Hex())

		for _, royalty := range expectedRoyalties {
			royaltyRegexString := fmt.Sprintf("%s: %s\\(address: %s, fee: %s, name: %s\\)", royalty.Name, contractRoyaltyNameRegex, royalty.Address, royalty.Fee, royalty.Name)

			assert.Regexp(t, regexp.MustCompile(royaltyRegexString), transactionLogs[0])
		}
	})
}

func TestCryptoyRoyaltyUpdates(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)
	t.Run("Should be able to update a royalty", func(t *testing.T) {
		// ======== create royalty
		expectedRoyalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

		// ======== read royalty.  No guarantee of order for royalties printed in log
		transactionLogs := CryptoysReadRoyalties(t, b, contracts, cryptoysSigner)

		assert.Len(t, transactionLogs, 1)

		contractRoyaltyNameRegex := fmt.Sprintf("A\\.%s\\.Cryptoys\\.Royalty", contracts.cryptoys.Hex())

		royaltyRegexString := fmt.Sprintf("%s: %s\\(address: %s, fee: %s, name: %s\\)", expectedRoyalty.Name, contractRoyaltyNameRegex, expectedRoyalty.Address, expectedRoyalty.Fee, expectedRoyalty.Name)

		assert.Regexp(t, regexp.MustCompile(royaltyRegexString), transactionLogs[0])

		// ======== update royalty
		expectedRoyalty.Fee, _ = cadence.NewUFix64(fmt.Sprintf("%f", rand.Float64()/100))
		expectedRoyalty.Address = cadence.BytesToAddress([]byte(strconv.Itoa(rand.Intn(999))))
		CryptoysUpsertRoyalty(t, b, contracts, cryptoysSigner, expectedRoyalty)

		// ======== read royalty again.  No guarantee of order for royalties printed in log
		transactionLogs = CryptoysReadRoyalties(t, b, contracts, cryptoysSigner)

		assert.Len(t, transactionLogs, 1)

		contractRoyaltyNameRegex = fmt.Sprintf("A\\.%s\\.Cryptoys\\.Royalty", contracts.cryptoys.Hex())

		royaltyRegexString = fmt.Sprintf("%s: %s\\(address: %s, fee: %s, name: %s\\)", expectedRoyalty.Name, contractRoyaltyNameRegex, expectedRoyalty.Address, expectedRoyalty.Fee, expectedRoyalty.Name)

		assert.Regexp(t, regexp.MustCompile(royaltyRegexString), transactionLogs[0])

	})
}

func TestAddFlowToAccount(t *testing.T) {
	b := newEmulator()
	contracts, _ := CryptoysDeployContracts(b, t)

	keyGenerator := test.AccountKeyGenerator()
	userKey1            := keyGenerator.New()
	userKey2            := keyGenerator.New()

	t.Run("Should be able add funds to account", func(t *testing.T) {
		result := CryptoysSetupAccount(t, b, userKey1, userKey2, contracts, 0)
		// ======== Verify 'sealed' event and receive new account's address
		var address flow.Address
		for _, event := range result.Events {
			if event.Type != flow.EventAccountCreated {
				continue
			}

			address = flow.AccountCreatedEvent(event).Address()
		}
		assert.Len(t, result.Events, 8)

		assert.False(t, address == flow.EmptyAddress)

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(address))},
		)
		assert.EqualValues(t, cadence.NewInt(0), length)

		account, err := b.GetAccount(address)
		assert.NoError(t, err)

		newAccountFunds, _ := cadence.NewUFix64FromParts(0, uint(DEFAULT_ACCOUNT_BALANCE))
		assert.EqualValues(t, newAccountFunds.ToGoValue().(uint64), account.Balance)

		extraAmount := rand.Intn(10)
		SendFlowToAccount(t, b, contracts, address, uint(extraAmount))

		account, err = b.GetAccount(address)
		assert.NoError(t, err)

		expectedAccountBalance := uint64(DEFAULT_ACCOUNT_BALANCE) + uint64(extraAmount)
		assert.Equal(t, expectedAccountBalance, account.Balance)
	})
}

func TestComposeNFT(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)
	royalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

	keyGenerator := test.AccountKeyGenerator()
	userKey1            := keyGenerator.New()
	userKey2            := keyGenerator.New()

	// create a new Collection
	t.Run("Should be able to create a new empty NFT Collection - with extra funds", func(t *testing.T) {
		flowTransferAmount, expectedAmountInAccount := mockNewAccountFlowValues()
		result := CryptoysSetupAccount(t, b, userKey1, userKey2, contracts, flowTransferAmount)
		// ======== Verify 'sealed' event and receive new account's address
		var address flow.Address
		for _, event := range result.Events {
			if event.Type != flow.EventAccountCreated {
				continue
			}

			
			address = flow.AccountCreatedEvent(event).Address()
		}

		assert.False(t, address == flow.EmptyAddress)
		assert.Len(t, result.Events, 10) // -- this is how many events the contract emits when the transaction is successful

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(address))},
		)
		assert.EqualValues(t, cadence.NewInt(0), length)

		account, err := b.GetAccount(address)
		assert.NoError(t, err)

		assert.EqualValues(t, expectedAmountInAccount.ToGoValue().(uint64), account.Balance)
	})

		// create a new Collection
		t.Run("Should be able to create a new empty NFT Collection - without extra funds", func(t *testing.T) {
			result := CryptoysSetupAccount(t, b, userKey1, userKey2, contracts, 0)
			// ======== Verify 'sealed' event and receive new account's address
			var address flow.Address
			for _, event := range result.Events {
				if event.Type != flow.EventAccountCreated {
					continue
				}
	
				address = flow.AccountCreatedEvent(event).Address()
			}
			assert.Len(t, result.Events, 8) // -- this is how many events the contract emits when the transaction is successful

			assert.False(t, address == flow.EmptyAddress)
	
			length := executeScriptAndCheck(
				t,
				b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
				[][]byte{jsoncdc.MustEncode(cadence.NewAddress(address))},
			)
			assert.EqualValues(t, cadence.NewInt(0), length)
	
			account, err := b.GetAccount(address)
			assert.NoError(t, err)

			expectedAccountBalance, _ := cadence.NewUFix64FromParts(0, uint(DEFAULT_ACCOUNT_BALANCE))
			assert.EqualValues(t, expectedAccountBalance.ToGoValue().(uint64), account.Balance)
		})

	// // transfer an NFT
	t.Run("Should be able to compose one NFT into another as an item", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)
		characterId, _ := CryptoysMint(t, b, contracts, cryptoysSigner, FakeNftMetadata(), nil, "", models.Royalties{royalty}, recipient)   // nft.ID = 0
		itemId, itemUuid := CryptoysMint(t, b, contracts, cryptoysSigner, FakeNftMetadata(), nil, "", models.Royalties{royalty}, recipient) // nft.ID = 1

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(2), length)

		// compose one cryptoy into another
		CryptoysComposeItem(t, b, recipient, contracts, cryptoysSigner, characterId, itemId, itemUuid, false)

		// Should only have one cryptoy in collection
		length = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(1), length)

		// Discompose item
		CryptoysDiscomposeItem(t, b, recipient, contracts, cryptoysSigner, characterId, itemId, itemUuid, false)

		length = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(2), length)
	})
}

func TestUpdateDisplay(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)

	t.Run("Should be able to update image of composed", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)

		characterNft := FakeNftMetadata()
		itemNft := FakeNftMetadata()

		characterId, _ := CryptoysMint(t, b, contracts, cryptoysSigner, characterNft, nil, "", models.Royalties{}, recipient) // nft.ID = 0
		itemId, itemUuid := CryptoysMint(t, b, contracts, cryptoysSigner, itemNft, nil, "", models.Royalties{}, recipient)    // nft.ID = 1

		CryptoysValidateMetadata(t, b, contracts, cryptoysSigner, characterId, characterNft, recipient)
		// compose one cryptoy into another
		CryptoysComposeItem(t, b, recipient, contracts, cryptoysSigner, characterId, itemId, itemUuid, false)

		CryptoysValidateMetadata(t, b, contracts, cryptoysSigner, characterId, characterNft, recipient)

		previousImageUrl := characterNft.Image
		characterNft.Image = faker.URL()
		previousVideoUrl := characterNft.Video
		characterNft.Video = faker.URL()
		CryptoysUpdateDisplay(t, b, contracts, cryptoysSigner, cadence.Address(recipient), characterId, cadence.String(characterNft.Image), cadence.String(previousImageUrl), cadence.String(characterNft.Video), cadence.String(previousVideoUrl))

		CryptoysValidateMetadata(t, b, contracts, cryptoysSigner, characterId, characterNft, recipient)

		// Discompose item
		CryptoysDiscomposeItem(t, b, recipient, contracts, cryptoysSigner, characterId, itemId, itemUuid, false)

		CryptoysUpdateDisplay(t, b, contracts, cryptoysSigner, cadence.Address(recipient), characterId, "", cadence.String(previousImageUrl), "", cadence.String(previousVideoUrl))

		characterNft.Image = previousImageUrl
		characterNft.Video = previousVideoUrl
		CryptoysValidateMetadata(t, b, contracts, cryptoysSigner, characterId, characterNft, recipient)

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(2), length)
	})
}

func TestBorrowComposedNFT(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)
	royalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

	// // transfer an NFT
	t.Run("Should be able to read bucket data of composed NFT", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)

		characterId, _ := CryptoysMint(t, b, contracts, cryptoysSigner, FakeNftMetadata(), nil, "", models.Royalties{royalty}, recipient)
		itemNft := FakeNftMetadata()
		itemId, itemUuid := CryptoysMint(t, b, contracts, cryptoysSigner, itemNft, nil, "", models.Royalties{royalty}, recipient)

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(2), length)

		// compose one cryptoy into another
		CryptoysComposeItem(t, b, recipient, contracts, cryptoysSigner, characterId, itemId, itemUuid, false)

		// Should only have one cryptoy in collection
		length = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient))},
		)
		assert.EqualValues(t, cadence.NewInt(1), length)

		// ======== Read bucket item metadata
		response := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysReadBucketItem),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient)),
				jsoncdc.MustEncode(cadence.String("items")),
				jsoncdc.MustEncode(characterId),
				jsoncdc.MustEncode(itemUuid),
			},
		)

		expectedMetadataValues := NftMetadataToMetadataView(itemNft)

		responseStruct, ok := response.(cadence.Struct)
		assert.True(t, ok)

		i := 0
		for _, value := range expectedMetadataValues {
			var cadenceValue cadence.Value
			if value == "" {
				cadenceValue = cadence.Value(nil)
			} else {
				cadenceValue = cadence.String(value)
			}

			assert.Equal(t, cadence.NewOptional(cadenceValue), responseStruct.Fields[i])
			i++
		}

		// ======== Read bucketType 'items' of nft
		response = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysReadBucketTypes),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient)),
				jsoncdc.MustEncode(characterId),
			},
		)

		expectedResponse := cadence.Array{
			Values: []cadence.Value{cadence.String("items")},
		}
		expectedResponse.ArrayType = cadence.NewVariableSizedArrayType(cadence.NewStringType())

		assert.Equal(t, expectedResponse, response)

		// ======== Read bucket ids of bucketType 'items' of nft
		response = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysReadBucketTypeIds),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(recipient)),
				jsoncdc.MustEncode(NewCadenceString("items")),
				jsoncdc.MustEncode(characterId),
			},
		)

		expectedResponse = cadence.Array{
			Values: []cadence.Value{itemUuid},
		}

		expectedResponse.ArrayType = cadence.NewVariableSizedArrayType(cadence.NewUInt64Type())

		assert.Equal(t, expectedResponse, response)
	})
}

var DEFAULT_ACCOUNT_BALANCE = 100_000 // -- this is equivalent to 0.001 flow which is how much is transferred from the account making the transaction to the new account being created.

func mockNewAccountFlowValues() (amountToDeposit cadence.UFix64, expectedAmountInAccount cadence.UFix64) {
	newAccountDefaultTransfer := DEFAULT_ACCOUNT_BALANCE
	randomAmount := rand.Intn(100_000_000)  // -- this is a random number that will converted to a cadence argument to be used as the Flow amount to be deposited into the new account.
	amountToDeposit, _ = cadence.NewUFix64FromParts(0, uint(randomAmount))
	expectedAmountInAccount, _ = cadence.NewUFix64FromParts(0, uint(randomAmount+newAccountDefaultTransfer))
	return
}

func TestTransferNFT(t *testing.T) {
	b := newEmulator()

	contracts, cryptoysSigner := CryptoysDeployContracts(b, t)
	royalty := CryptoysCreateRoyalty(t, b, contracts, cryptoysSigner)

	keyGenerator := test.AccountKeyGenerator()
	userKey1     := keyGenerator.New()
	userKey2     := keyGenerator.New()
	userAddress  := flow.Address{}

	// create a new Collection
	t.Run("Should be able to create a new empty NFT Collection", func(t *testing.T) {
		flowTransferAmount, expectedAmountInAccount := mockNewAccountFlowValues()

		result := CryptoysSetupAccount(t, b, userKey1, userKey2, contracts, flowTransferAmount)
		// ======== Verify 'sealed' event and receive new account's address
		for _, event := range result.Events {
			if event.Type != flow.EventAccountCreated {
				continue
			}

			
			userAddress = flow.AccountCreatedEvent(event).Address()
		}

		assert.False(t, userAddress == flow.EmptyAddress)

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)

		assert.EqualValues(t, cadence.NewInt(0), length)

		account, err := b.GetAccount(userAddress)
		assert.NoError(t, err)

		assert.EqualValues(t, expectedAmountInAccount.ToGoValue().(uint64), account.Balance)
	})

	t.Run("Should not be able to withdraw an NFT that does not exist in a collection", func(t *testing.T) {
		nonExistentAssetID := cadence.NewUInt64(3333)
		CryptoysTransferItem(
			t, b,
			contracts.cryptoys,
			contracts, cryptoysSigner,
			nonExistentAssetID, userAddress, true,
		)
	})

	// transfer an NFT
	t.Run("Should be able to withdraw an NFT and deposit to another accounts collection", func(t *testing.T) {
		recipient := NewCryptoysAccount(t, b, contracts)
		id, _ := CryptoysMint(t, b, contracts, cryptoysSigner, FakeNftMetadata(), nil, "", models.Royalties{royalty}, recipient)

		length := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)
		assert.EqualValues(t, cadence.NewInt(0), length)

		currentAccountRemainingStorage := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(readUserAccountRemainingStorage),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)
		assert.NotZero(t, currentAccountRemainingStorage)

		CryptoysTransferItem(t, b, recipient, contracts, cryptoysSigner, id, userAddress, false)

		length = executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(cryptoysInspectCollectionLenPath),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)
		assert.EqualValues(t, cadence.NewInt(1), length)

		updatedAccountRemainingStorage := executeScriptAndCheck(
			t,
			b, contracts.ReplaceInCode(readUserAccountRemainingStorage),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)
		assert.Less(t, updatedAccountRemainingStorage, currentAccountRemainingStorage)
	})
}

func ensureHexAddress(address string) string {
	if strings.HasPrefix(address, "0x") {
		return address
	}

	return "0x" + address
}

type ContractAddresses struct {
	nonFungibleToken     flow.Address
	metadataViews        flow.Address
	cryptoys             flow.Address
	iCryptoys            flow.Address
	cryptoysMetadataView flow.Address
	fungibleToken        flow.Address
	toyken               flow.Address
	flowToken            flow.Address
}

func replaceContractAddress(contract, address string, code []byte) []byte {
	replacement := fmt.Sprintf("import %s from %s", contract, ensureHexAddress(address))
	regex := regexp.MustCompile(fmt.Sprintf(`import\s+%s\s+from.*`, contract)) // -- matches contract file path or
	return []byte(regex.ReplaceAllString(string(code), replacement))
}

func (ca ContractAddresses) ReplaceInCode(codePath string) []byte {
	code := readFile(codePath)

	if ca.nonFungibleToken != flow.EmptyAddress {
		code = replaceContractAddress("NonFungibleToken", ca.nonFungibleToken.Hex(), code)
	}
	if ca.metadataViews != flow.EmptyAddress {
		code = replaceContractAddress("MetadataViews", ca.metadataViews.Hex(), code)
	}
	if ca.cryptoys != flow.EmptyAddress {
		code = replaceContractAddress("Cryptoys", ca.cryptoys.Hex(), code)
	}
	if ca.iCryptoys != flow.EmptyAddress {
		code = replaceContractAddress("ICryptoys", ca.iCryptoys.Hex(), code)
	}
	if ca.cryptoysMetadataView != flow.EmptyAddress {
		code = replaceContractAddress("CryptoysMetadataView", ca.cryptoysMetadataView.Hex(), code)
	}
	if ca.fungibleToken != flow.EmptyAddress {
		code = replaceContractAddress("FungibleToken", ca.fungibleToken.Hex(), code)
	}
	if ca.toyken != flow.EmptyAddress {
		code = replaceContractAddress("Toyken", ca.toyken.Hex(), code)
	}
	if ca.flowToken != flow.EmptyAddress {
		code = replaceContractAddress("FlowToken", ca.flowToken.Hex(), code)
	}

	return code
}

func NewCadenceString(value string) cadence.Value {
	newValue, err := cadence.NewString(value)

	if err != nil {
		panic(err)
	}

	return newValue
}

func AssertStruct(t *testing.T, test cadence.Struct, actual cadence.Struct) {
	for i, testField := range test.Fields {
		AssertCadenceValue(t, testField, actual.Fields[i])
	}
}

func AssertDictionary(t *testing.T, test cadence.Dictionary, actual cadence.Dictionary) {
	for _, testPair := range test.Pairs {
		exists := false
		for _, actualPair := range actual.Pairs {
			if actualPair.Key == testPair.Key {
				exists = true

				AssertCadenceValue(t, testPair.Value, actualPair.Value)
			}
		}
		assert.True(t, exists)
	}
}

func AssertArray(t *testing.T, test cadence.Array, actual cadence.Array) {
	for i, testValue := range test.Values {
		AssertCadenceValue(t, testValue, actual.Values[i])
	}
}

// TODO: this can be extended for all Cadence Value types although these are the only ones we currently use.
func AssertCadenceValue(t *testing.T, test interface{}, actual interface{})	 {
	switch testValue := test.(type) {
	case cadence.Struct:
		if actualStruct, ok := actual.(cadence.Struct); ok {
			AssertStruct(t, testValue, actualStruct)
		} else {
			assert.Fail(t, fmt.Sprintf("cadence value type (%T) is not type (cadence.Struct)", actual))
		}
	case cadence.Dictionary:
		if actualDictionary, ok := actual.(cadence.Dictionary); ok {
			AssertDictionary(t, testValue, actualDictionary)
		} else {
			assert.Fail(t, fmt.Sprintf("cadence value type (%T) is not type (cadence.Dictionary)", actual))
		}
	case cadence.Array:
		if actualArray, ok := actual.(cadence.Array); ok {
			AssertArray(t, testValue, actualArray)
		} else {
			assert.Fail(t, fmt.Sprintf("cadence value type (%T) is not type (cadence.Array)", actual))
		}
	default:
		assert.True(t, test == actual, actual, fmt.Sprintf("cadence test %T expected value value (%v) is not equal to the actual value (%v)", testValue, test, actual))
	}
}

func AssertEvents(t *testing.T, events []flow.Event, tests []EventsTest) {
	assert.Len(t, events, len(tests))

	for i, test := range tests {
		event := events[i]

		assert.Equal(t, test.eventName, event.Value.EventType.QualifiedIdentifier)
		assert.Lenf(t, event.Value.Fields, len(test.arguments), "event %s should have %d arguments", test.eventName, len(test.arguments))

		for j, testArg := range test.arguments {
			eventArg := event.Value.Fields[j]

			if testValue, ok := testArg.(cadence.Value); ok {
				AssertCadenceValue(t, testValue, eventArg)
			} else if testValueType, ok := testArg.(reflect.Type); ok { // if test value is 'reflect.Type' then the value was unknown, Check if types are the same.
				assert.Equalf(t, testValueType, reflect.TypeOf(eventArg), "Event (%s) argument (%T) is incorrect type (%T)", test.eventName, testValueType, eventArg)
			} else {
				assert.Failf(t, "unsupported arg type", "event %s unsupported event argument type %v.  unable to test %v",test.eventName, testArg, testValue)
			}
		}
	}
}
