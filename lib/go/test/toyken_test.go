package test

import (
	"testing"

	"github.com/onflow/cadence"
	jsoncdc "github.com/onflow/cadence/encoding/json"
	emulator "github.com/onflow/flow-emulator"
	"github.com/onflow/flow-go-sdk"
	sdk "github.com/onflow/flow-go-sdk"
	"github.com/onflow/flow-go-sdk/crypto"
	"github.com/onflow/flow-go-sdk/templates"
	"github.com/onflow/flow-go-sdk/test"
	"github.com/stretchr/testify/assert"

	ft_contracts "github.com/onflow/flow-ft/lib/go/contracts"
)

const (
	toykenRootPath           = "../../.."
	toykenContractPath       = toykenRootPath + "/contracts/Toyken.cdc"
	toykenSetupAccountPath   = toykenRootPath + "/transactions/toyken/setup_account.cdc"
	toykenTransferTokensPath = toykenRootPath + "/transactions/toyken/transfer_tokens.cdc"
	toykenMintTokensPath     = toykenRootPath + "/transactions/toyken/mint_tokens.cdc"
	toykenBurnTokensPath     = toykenRootPath + "/transactions/toyken/burn_tokens.cdc"
	toykenGetBalancePath     = toykenRootPath + "/scripts/toyken/get_balance.cdc"
	toykenGetSupplyPath      = toykenRootPath + "/scripts/toyken/get_supply.cdc"
)

func ToykenDeployContracts(b *emulator.Blockchain, t *testing.T) (ContractAddresses, crypto.Signer) {
	accountKeys := test.AccountKeyGenerator()

	// Should be able to deploy a contract as a new account with no keys.
	fungibleTokenCode := loadFungibleToken()
	fungibleAddr, err := b.CreateAccount(
		[]*flow.AccountKey{},
		[]templates.Contract{templates.Contract{
			Name:   "FungibleToken",
			Source: string(fungibleTokenCode),
		}},
	)
	assert.NoError(t, err)

	_, err = b.CommitBlock()
	assert.NoError(t, err)

	toykenAccountKey, toykenSigner := accountKeys.NewWithSigner()
	contracts := ContractAddresses{fungibleToken: fungibleAddr}
	toykenCode := contracts.ReplaceInCode(toykenContractPath)

	toykenAddr, err := b.CreateAccount(
		[]*flow.AccountKey{toykenAccountKey},
		[]templates.Contract{templates.Contract{
			Name:   "Toyken",
			Source: string(toykenCode),
		}},
	)
	assert.NoError(t, err)

	_, err = b.CommitBlock()
	assert.NoError(t, err)

	contracts.toyken = toykenAddr

	// Simplify testing by having the contract address also be our initial Vault.
	ToykenSetupAccount(t, b, toykenAddr, toykenSigner, contracts)

	return contracts, toykenSigner
}

func ToykenSetupAccount(t *testing.T, b *emulator.Blockchain, userAddress sdk.Address, userSigner crypto.Signer, contracts ContractAddresses) {
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(toykenSetupAccountPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(userAddress)

	serviceKeySigner, _ := b.ServiceKey().Signer()
	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, userAddress},
		[]crypto.Signer{serviceKeySigner, userSigner},
		false,
	)
}

func ToykenCreateAccount(t *testing.T, b *emulator.Blockchain, contracts ContractAddresses) (sdk.Address, crypto.Signer) {
	userAddress, userSigner, _ := createAccount(t, b)
	ToykenSetupAccount(t, b, userAddress, userSigner, contracts)
	return userAddress, userSigner
}

func ToykenMint(t *testing.T, b *emulator.Blockchain, contracts ContractAddresses, toykenSigner crypto.Signer, recipientAddress flow.Address, amount string, shouldRevert bool) {
	tx := flow.NewTransaction().
		SetScript(contracts.ReplaceInCode(toykenMintTokensPath)).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(contracts.toyken)

	_ = tx.AddArgument(cadence.NewAddress(recipientAddress))
	_ = tx.AddArgument(CadenceUFix64(amount))

	serviceKeySigner, _ := b.ServiceKey().Signer()
	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, contracts.toyken},
		[]crypto.Signer{serviceKeySigner, toykenSigner},
		shouldRevert,
	)

}

func TestToykenDeployment(t *testing.T) {
	b := newEmulator()

	contracts, _ := ToykenDeployContracts(b, t)

	t.Run("Should have initialized Supply field correctly", func(t *testing.T) {
		supply := executeScriptAndCheck(t, b, contracts.ReplaceInCode(toykenGetSupplyPath), nil)
		assert.EqualValues(t, CadenceUFix64("0.0"), supply)
	})
}

func TestToykenSetupAccount(t *testing.T) {
	b := newEmulator()

	t.Run("Should be able to create empty vault that does not affect supply", func(t *testing.T) {
		contracts, _ := ToykenDeployContracts(b, t)

		userAddress, _ := ToykenCreateAccount(t, b, contracts)

		userBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(userAddress))},
		)
		assert.EqualValues(t, CadenceUFix64("0.0"), userBalance)

		supply := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetSupplyPath),
			nil,
		)
		assert.EqualValues(t, CadenceUFix64("0.0"), supply)
	})
}

func TestToykenMinting(t *testing.T) {
	b := newEmulator()

	contracts, toykenSigner := ToykenDeployContracts(b, t)

	userAddress, _ := ToykenCreateAccount(t, b, contracts)

	t.Run("Should not be able to mint zero tokens", func(t *testing.T) {
		ToykenMint(t, b, contracts, toykenSigner, userAddress, "0.0", true)
	})

	t.Run("Should be able to mint tokens, deposit, update balance and total supply", func(t *testing.T) {
		ToykenMint(t, b, contracts, toykenSigner, userAddress, "50.0", false)

		// Assert that vault balance is correct
		userBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(userAddress))},
		)
		assert.EqualValues(t, CadenceUFix64("50.0"), userBalance)

		// Assert that total supply is correct
		supply := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetSupplyPath),
			nil,
		)
		assert.EqualValues(t, CadenceUFix64("50.0"), supply)
	})
}

func TestToykenTransfers(t *testing.T) {
	b := newEmulator()

	contracts, toykenSigner := ToykenDeployContracts(b, t)

	userAddress, _ := ToykenCreateAccount(t, b, contracts)

	ToykenMint(t, b, contracts, toykenSigner, contracts.toyken, "1000.0", false)

	t.Run("Should not be able to withdraw more than the balance of the vault", func(t *testing.T) {
		tx := flow.NewTransaction().
			SetScript(contracts.ReplaceInCode(toykenTransferTokensPath)).
			SetGasLimit(100).
			SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
			SetPayer(b.ServiceKey().Address).
			AddAuthorizer(contracts.toyken)

		_ = tx.AddArgument(CadenceUFix64("30000.0"))
		_ = tx.AddArgument(cadence.NewAddress(userAddress))

		serviceKeySigner, _ := b.ServiceKey().Signer()
		signAndSubmit(
			t, b, tx,
			[]flow.Address{b.ServiceKey().Address, contracts.toyken},
			[]crypto.Signer{serviceKeySigner, toykenSigner},
			true,
		)

		// Assert that vault balances are correct

		toykenBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(contracts.toyken))},
		)
		assert.EqualValues(t, CadenceUFix64("1000.0"), toykenBalance)

		userBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(userAddress))},
		)
		assert.EqualValues(t, CadenceUFix64("0.0"), userBalance)
	})

	t.Run("Should be able to withdraw and deposit tokens from a vault", func(t *testing.T) {
		tx := flow.NewTransaction().
			SetScript(contracts.ReplaceInCode(toykenTransferTokensPath)).
			SetGasLimit(100).
			SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
			SetPayer(b.ServiceKey().Address).
			AddAuthorizer(contracts.toyken)

		_ = tx.AddArgument(CadenceUFix64("300.0"))
		_ = tx.AddArgument(cadence.NewAddress(userAddress))

		serviceKeySigner, _ := b.ServiceKey().Signer()
		signAndSubmit(
			t, b, tx,
			[]flow.Address{b.ServiceKey().Address, contracts.toyken},
			[]crypto.Signer{serviceKeySigner, toykenSigner},
			false,
		)

		// Assert that vault balances are correct

		toykenBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(contracts.toyken))},
		)
		assert.EqualValues(t, CadenceUFix64("700.0"), toykenBalance)

		userBalance := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetBalancePath),
			[][]byte{jsoncdc.MustEncode(cadence.Address(userAddress))},
		)
		assert.EqualValues(t, CadenceUFix64("300.0"), userBalance)

		supply := executeScriptAndCheck(
			t, b,
			contracts.ReplaceInCode(toykenGetSupplyPath),
			nil,
		)
		assert.EqualValues(t, CadenceUFix64("1000.0"), supply)
	})
}

func loadFungibleToken() []byte {
	return ft_contracts.FungibleToken()
}
